from flask import request, render_template, redirect, url_for, abort
from pony.orm import db_session, ObjectNotFound, flush

from . import app
from .forms import *
from .data_base import *
import calendar

# ********************* Tests ****************************
@app.route('/tests', methods=['GET', 'POST'])
@db_session
def test_add():
    form = IMForm()

    return render_template('tests/add.html', form=form)

@app.route('/tests/2', methods=['GET', 'POST'])
@db_session
def test_add2():
    form = ContactForm2()

    return render_template('tests/add.html', form=form)

@app.route('/tests/grading', methods=['GET', 'POST'])
@db_session
def test_grading():
    form = StudentForm()
    students = Student.select()
    return render_template('tests/list.html', students=students, form=form)

#************************Groups****************************
@app.route('/groups')
@db_session
def groups_list():
    return render_template('groups/list.html', groups=Group.select().order_by(Group.id))

@app.route('/group/add', methods=['GET', 'POST'])
@db_session
def group_add():
    form = GroupForm()
    if form.validate_on_submit():
        Group(name=form.name.data)
        flush()
        return redirect(url_for('groups_list'))
    return render_template('groups/add.html', form=form)


@app.route('/group/show/<int:id>')
@db_session
def group_show(id):
    group = Group[id]
    students = group.students
    teachers = group.teachers
    return render_template('groups/show.html', group=group, students=students, teachers=teachers)


@app.route('/group/edit/<int:id>', methods=['GET', 'POST'])
@app.route('/group/edit/<int:id>/<string:cmd1>/<string:cmd2>/<int:object_id>', methods=['GET', 'POST'])
@db_session
def group_edit(id, cmd1=None, cmd2=None, object_id=None):
    form = GroupForm()
    group = Group[id]

    if form.validate_on_submit():
        Group[id].name = form.name.data
        flush()
    elif cmd1 == 'student':
        if cmd2 == 'delete':
            group.students.remove(Student[object_id])
            flush()
        elif cmd2 == 'add':
            group.students.add(Student[object_id])
            flush()
    elif cmd1 == 'teacher':
        if cmd2 == 'delete':
            group.teachers.remove(Teacher[object_id])
            flush()
        elif cmd2 == 'add':
            group.teachers.add(Teacher[object_id])
            flush()

    students_in_group = Group[id].students
    students_outside_group = list(Student.select()[:])
    for s in students_in_group:
        students_outside_group.remove(s)

    teachers_in_group = Group[id].teachers
    teachers_outside_group = list(Teacher.select()[:])
    for t in teachers_in_group:
        teachers_outside_group.remove(t)

    return render_template('groups/edit.html',
                           group=group, form=form,
                           students_in_group=students_in_group,
                           students_outside_group=students_outside_group,
                           teachers_in_group=teachers_in_group,
                           teachers_outside_group=teachers_outside_group)


@app.route('/group/grading/<int:group_id>/', methods=['GET', 'POST'])
@app.route('/group/grading/<int:group_id>/<int:course_id>', methods=['GET', 'POST'])
@app.route('/group/grading/<int:group_id>/<int:course_id>/<int:block_id>', methods=['GET', 'POST'])
@app.route('/group/grading/<int:group_id>/<int:course_id>/<int:block_id>/<int:task_id>', methods=['GET', 'POST'])
@app.route('/group/grading/<int:group_id>/<int:course_id>/<int:block_id>/<int:task_id>/<int:student_id>', methods=['GET', 'POST'])
@db_session
def group_grading(group_id, course_id=None, block_id=None, task_id=None, student_id=None):

    group = Group[group_id]
    courses = group.courses

    if not course_id:
        return render_template('groups/grading/courses_list.html',
                               group=group,
                               courses=courses)
    elif not block_id:
        course = Course[course_id]
        blocks = course.list_of_blocks
        return render_template('groups/grading/blockes_list.html',
                               group=group,
                               course=course,
                               blocks=blocks)
    elif not task_id:
        course = Course[course_id]
        block = Block[block_id]
        tasks = block.list_of_tasks
        return render_template('groups/grading/tasks_list.html',
                               group=group,
                               course=course,
                               block=block,
                               tasks=tasks)
    else:
        form = MarkForm()

        if form.validate_on_submit():
            TasksResults(task_id=task_id, student_id=student_id, mark=form.mark.data)
            flush()

        course = Course[course_id]
        block = Block[block_id]
        task = Task[task_id]
        studets = group.students

        return render_template('groups/grading/marks_filling.html',
                               group=group,
                               course=course,
                               block=block,
                               task=task,
                               students=studets,
                               form=form)


@app.route('/group/add/<int:id>')
@db_session
def group_delete(id):
    pass

#************************Students****************************
@app.route('/students')
@db_session
def students_list():
    return render_template('students/list.html', students=Student.select().order_by(Student.id))

@app.route('/students/add', methods=['GET', 'POST'])
@db_session
def student_add():
    form = StudentForm()
    if form.validate_on_submit():
        Student(full_name=form.full_name.data)
        flush()
        return redirect(url_for('students_list'))
    return render_template('students/add.html', form=form)

@app.route('/students/edit/<int:id>', methods=['GET', 'POST'])
@db_session
def student_edit(id):
    student = Student[id]
    form = StudentForm(obj=student)

    if form.validate_on_submit():
        Student[id].full_name = form.full_name.data
        flush()
        return redirect(url_for('students_list'))
    else:
        return render_template('students/edit.html', student=student, form=form)

@app.route('/students/delete/<int:id>')
@db_session
def student_delete(id):
    try:
        Student[id].delete()
        return render_template('students/list.html', students=Student.select().order_by(Student.id))
    except ObjectNotFound:
        abort(404)


@app.route('/students/show/<int:id>')
@db_session
def student_show(id):
    pass



