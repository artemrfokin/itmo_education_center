from flask import Flask
from .data_base import db
from flask_bootstrap import Bootstrap
from flask_wtf import CSRFProtect

app = Flask(__name__)

"""
интерпритатор env bin python
"""

app.config.from_pyfile('config.py')

Bootstrap(app)
CSRFProtect(app)

from .views import *


# @app.route('/hello') # обработчик url ve
# def hello():
#     return 'Hello Flask-bitch'

"""
Как запустить. Начиная с первой версии 
cmd flask run
"""