from pony.orm import *
from pony.orm.ormtypes import LongStr
from datetime import datetime


tasks = dict(title=['login_required', 'organaser', 'See batleship', 'Educational center'],
             discription=['To write app which requires auth-n for enter',
                          'To write organaser app',
                          'To write game "See batleship"',
                          'To write CRM for Educational Center '])


db = Database()

class Group(db.Entity):
    name = Required(str, 30)
    courses = Set('Course')
    teachers = Set('Teacher')
    students = Set('Student')
    #Links
    # working_days = Set('WorkingDay')
    # location = Optional('Location')



class Teacher(db.Entity):
    full_name = Required(str, 50)
    specialisation = Required(str, 100)
    experience = Optional(str, 100)
    #Links
    group = Set('Group')

teachers = dict(full_name=['Riril Verceti', 'Isaak Newtone', 'Sergey Brin'],
                specialisation=['Python, PHP', 'Classic physics', 'Making money'])


class Student(db.Entity):
    full_name = Required(str, 100)
    contract = Optional('Contract')
    #course_results = Set('Mark') # Результаты по каждому курсу. Внутри каждого курса расшифровка какие
    # баллы за блок, за задание. Для каждого задания хранится не только оценка, но и решение, которое сделал
    # студент. А по хорошему история оценок = список (присланное решение, оценка, дата сдачи). Не в этот раз.
    # course_results = Set('Course_result')
    # создатель ведомости успеваемости по конкретному курсу:
    #  1) анализирует структуру курса (знает сколько уровней заранее) - код курса, название курса, оценка
    # код блока, название блока, оценка, код задания, название задания, оценка.
    # пишет пары. Нужно задать определенную логику по формированию идентификаторов курсов, блоков, заданий
    # после этого можно будет создавать иерархию заданий и прописывать для каждого уровня оценку.
    #Links
    groups = Set('Group')
    tasks_results = Set('TasksResults')
    blocks_results = Set('BlocksResults')
    course_results = Set('CourseResults')

studets = dict(full_name=['Artem Fokin', 'Denis Sumak', 'Mark Tolubaev'])

# class Coure_result(db.Entity):
#     course = Required('Course')
#     mark = Optional('Mark')


class WorkingDay(db.Entity):
    date = datetime
    time = datetime


class Location(db.Entity):
    name = Required(LongStr)
    #Links
    # groups = Set('Group')

class Contract(db.Entity):
    name = Required(str)
    period = Required(datetime)
    payment_status = Required(str, default='Not payed')
    #Links
    students = Required('Student')

class Task(db.Entity):
    title = Required(str, 100)
    discription = Required(str, 100)
    #marks = Set('Mark')
    # Links
    blockes = Optional('Block')
    results = Set('TasksResults')

class Block(db.Entity):
    name = Required(str, 100)
    #Links
    courses = Optional('Course')
    list_of_tasks = Set('Task')
    blocks_result = Set('BlocksResults')

class Course(db.Entity): # набор знаний
    name = Required(str, 100)
    list_of_blocks = Set('Block')
    num_lessons = Required(int)
    #Links
    groups = Set('Group')
    course_results = Set('CourseResults')


class TasksResults(db.Entity):
    task_id = Required('Task')
    student_id = Required('Student')
    mark = Optional(float)
    PrimaryKey(task_id, student_id)

class BlocksResults(db.Entity):
    block_id = Required('Block')
    student_id = Required('Student')
    mark = Optional(float)
    PrimaryKey(block_id, student_id)

    @classmethod
    def update_table(cls):
        for r in BlocksResults.select():
            r.delete()

        obj_task_list = select(t for t in TasksResults)[:]
        for obj in obj_task_list:
            task = obj.task_id
            student = obj.student_id
            mark = obj.mark

            obj_block_list = select(b for b in Block)[:]
            for obj in obj_block_list:
                if task in obj.list_of_tasks:
                    block = obj

            if not select(b for b in BlocksResults if b.block_id == block and b.student_id == student):
                BlocksResults(block_id=block, student_id=student, mark=mark)
            else:
                BlocksResults[block, student].mark += mark

class CourseResults(db.Entity):
    course_id = Required('Course')
    student_id = Required('Student')
    mark = Optional(float)
    PrimaryKey(course_id, student_id)

    @classmethod
    def update_table(cls):
        for r in CourseResults.select():
            r.delete()

        obj_block_list = select(b for b in BlocksResults)[:]
        for obj in obj_block_list:
            block = obj.block_id
            student = obj.student_id
            mark = obj.mark

            obj_course_list = select(c for c in Course)[:]
            for obj in obj_course_list:
                if block in obj.list_of_blocks:
                    course = obj

            if not select(r for r in CourseResults if r.course_id == course and r.student_id == student):
                CourseResults(course_id=course, student_id=student, mark=mark)
            else:
                CourseResults[course, student].mark += mark


db.bind('sqlite', 'edu_centr.sqlite', create_db=True)
db.generate_mapping(create_tables=True)


# if __name__ == '__main__':
#     with db_session:
        # Student(full_name="Petr Petrov")
        # Student.select().show()
        # print(Group[1])
        # Student.select(lambda groups: not Student.groups).show()
        # # Tasks filling
        # for i in range(len(tasks['title'])):
        #     Task(title=tasks['title'][i], discription=tasks['discription'][i])
        #
        # # Blockes filing
        # blocks = dict(name=['Functional programming', 'Object-oriented programming'],
        #               list_of_tasks=[[Task[1], Task[2]], [Task[3], Task[4]]])
        # for i in range(len(blocks['name'])):
        #     Block(name=blocks['name'][i], list_of_tasks=blocks['list_of_tasks'][i])
        #
        # # Courses filing
        # Course(name='Python', num_lessons=24, list_of_blocks = [Block[3], Block[4]])
        #
        # #Teachers filling
        # groups = dict(full_name=['Riril Verceti', 'Isaak Newtone', 'Sergey Brin'],
        #                 specialisation=['Python, PHP', 'Classic physics', 'Making money'])
        # for i in range(len(groups['full_name'])):
        #     Teacher(full_name=groups['full_name'][i], specialisation=groups['specialisation'][i])
        #
        # #Students filing
        # studets = dict(full_name=['Artem Fokin', 'Denis Sumak', 'Mark Tolubaev'])
        # for i in range(len(studets['full_name'])):
        #     Student(full_name=studets['full_name'][i])

        # #Group filling
        # Group(name='Python_18_1', courses=[Course[1]])

        # TaskResults_filling
        # results_dict = dict(task_id=[1, 2, 3, 4], student_id=[1, 2, 3], mark=5)
        # for i in range(len(results_dict['student_id'])):
        #     for j in range(len(results_dict['task_id'])):
        #         TasksResults(task_id=results_dict['task_id'][j], student_id=results_dict['task_id'][i], mark=5)
        # for i in range(4):
        #     for j in range(3):
        #         if j == 0:
        #             mark = 5
        #         elif j == 1:
        #             mark = 4
        #         elif j == 2:
        #             mark = 3
        #         TasksResults[i + 1, j + 1].mark = mark

        # Group[1].students = select(s for s in Student)
        # Group[1].groups=[Teacher[1], Teacher[3]]

        # Calculating results
        # BlocksResults.update_table()
        # CourseResults.update_table()


        # print('\nTasks')
        # select(t for t in Task).show()
        # print('\nBlocks')
        # select(b for b in Block).show()
        # print('\nCourses')
        # select(c for c in Course).show()
        # print('\nTeachers')
        # select(t for t in Teacher).show()
        # print('\nStudents')
        # select(s for s in Student).show()
        # print('\nGroups:')
        # select(g for g in Group).show()
        # print('\nTaskResults:')
        # select(r for r in TasksResults).show()
        # print('\nBlocksResults:')
        # select(r for r in BlocksResults).show()
        # print('\nCourseResults:')
        # select(r for r in CourseResults).show()

        # print('\nTaskResults:')
        # print(select(t for t in TasksResults)[:])
        # print('\nGroup courses:')
        # print(select(g.courses for g in Group)[:])
        # print('\nGroup groups:')
        # print(select(g.groups for g in Group)[:])
        # print('\nGroup students:')
        # print(select(g.students for g in Group)[:])
        # print('\nResults:')
        # print(select(r for r in Results)[:])
        # print(Student.select()[:])
        # print(Student.select()[:])
         # Group[1].students.add(Student[10])

        # students_in_group = Group[1].students
        # for s in students_in_group:
        #     print(s)
        #
        # Group[1].courses.add(Course[3])



# class Mark(db.Entity):
#     title = Required(int)
#     # Links
#     task = Required('Task')


# class Administrator():
#     full_name = Required(str, 100)
#     responsibilities = list


# class Schedule():
#     calendar = datetime
#     working_days_list = list
#     working_hours_list = list

# class Client_contacts():
#     pass

# class CourseKnowledge():
#     pass
#
# class CourseEquipment():
#     pass
#
# class Consumables():
#     pass
#
# class FreeLesson():
#     pass
