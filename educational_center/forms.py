from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, FormField, SelectField, FieldList, Form, FloatField, HiddenField
from wtforms.validators import InputRequired, Length

class StudentForm(FlaskForm):
    full_name = StringField('', validators=[InputRequired(), Length(min=2, max=100)])


class GroupForm(FlaskForm):
    name = StringField('Новое имя группы', validators=[InputRequired(), Length(min=2, max=100)])


class MarkForm(FlaskForm):
    mark = FloatField('', validators=[InputRequired(), Length(min=1, max=1)])

class TelephoneForm(FlaskForm):
    country_code = IntegerField('Country Code', validators=[InputRequired(), Length(min=1, max=3)])
    area_code    = IntegerField('Area Code/Exchange', validators=[InputRequired(), Length(min=1, max=6)])
    number       = StringField('Number', validators=[InputRequired(), Length(min=2, max=100)])

class ContactForm(FlaskForm):
    first_name   = StringField()
    last_name    = StringField()
    mobile_phone = FormField(TelephoneForm)
    office_phone = FormField(TelephoneForm)

class IMForm(FlaskForm):
    protocol = SelectField(choices=[('aim', 'AIM'), ('msn', 'MSN')])
    # protocol = SelectField(choices=[1, 2, 3])
    username = StringField()

class ContactForm2(FlaskForm):
    first_name  = StringField()
    last_name   = StringField()
    im_accounts = FieldList(FormField(IMForm), min_entries=2)


